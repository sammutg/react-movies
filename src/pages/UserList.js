import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Card from '../components/Card';
import Header from '../components/Header';

const UserList = () => {
    const [listData, setListData] = useState([]);

    useEffect(() => {
        let moviesId = window.localStorage.movies ? window.localStorage.movies.split(',') : [];
        // console.log('🚀 ~ file: UserList.js ~ line 11 ~ useEffect ~ moviesId', moviesId);

        for (let i = 0; i < moviesId.length; i++) {
            const element = moviesId[i];
            console.log('🚀 ~ file: UserList.js ~ line 15 ~ useEffect ~ element', element);

            axios
                .get(
                    `https://api.themoviedb.org/3/movie/${element}?api_key=2a54330634530488e3d60a9816821c87&language=fr-FR`
                )
                .then((res) => setListData((listData) => [...listData, res.data]));
        }
    }, []);

    return (
        <div className="user-list-page">
            <Header />
            <h2>
                Favoris <span>💖</span>
            </h2>
            <div className="result">
                {listData.length > 0 ? (
                    listData.map((movie) => <Card movie={movie} key={movie.id} />)
                ) : (
                    <h2>Aucun coup de coeur pour le moment</h2>
                )}
            </div>
        </div>
    );
};

export default UserList;
