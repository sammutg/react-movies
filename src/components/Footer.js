import React from 'react';

const Footer = () => {
    const align = {
        textAlign: 'center',
    };

    return (
        <footer>
            <p style={align}>
                source:{' '}
                <a href="https://www.themoviedb.org" target="_blank" rel="noopener noreferrer">
                    tmdb
                </a>
            </p>
        </footer>
    );
};

export default Footer;
