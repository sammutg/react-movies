import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Card from './Card';

const Form = () => {
    const [moviesData, setMoviesData] = useState([]);

    const [search, setSearch] = useState('');

    const [sortGoodBad, setSortGoodBad] = useState(null);

    useEffect(() => {
        axios
            .get(
                `https://api.themoviedb.org/3/search/movie?api_key=2a54330634530488e3d60a9816821c87&query=${search}&language=en-US&page=1&include_adult=false`
            )
            .then((res) => setMoviesData(res.data.results));
    }, [search]);

    return (
        <div className="form-component">
            <div className="form-container">
                <form>
                    <input
                        type="text"
                        placeholder="Entrez le titre du film"
                        id="search-input"
                        onChange={(e) => setSearch(e.target.value)}
                    />
                    <input type="submit" value="Rechercher" />
                </form>
                <div className="btn-sort-container">
                    <div
                        className="btn-sort"
                        id="goodToBad"
                        onClick={() => setSortGoodBad('goodToBad')}
                    >
                        Top
                    </div>
                    <div
                        className="btn-sort"
                        id="badToGood"
                        onClick={() => setSortGoodBad('badToGood')}
                    >
                        Flop
                    </div>
                </div>
            </div>
            <div className="result">
                {moviesData
                    .slice(0, 12)
                    .sort((a, b) => {
                        if (sortGoodBad === 'goodToBad') {
                            return b.vote_average - a.vote_average;
                        } else if (sortGoodBad === 'badToGood') {
                            return a.vote_average - b.vote_average;
                        }
                    })
                    .map((movie) => (
                        <Card key={movie.id} movie={movie} />
                    ))}
            </div>
            <ul></ul>
        </div>
    );
};

export default Form;
