import React from 'react';

const Card = ({ movie }) => {
    const dateFormater = (date) => {
        let [yy, mm, dd] = date.split('-');
        return [dd, mm, yy].join('-');
    };

    const listGenres =
        movie.genre_ids && movie.genre_ids.map((number) => <li key={number}>{number}</li>);

    const addStorage = () => {
        let storedData = window.localStorage.movies ? window.localStorage.movies.split(',') : [];

        if (!storedData.includes(movie.id.toString())) {
            storedData.push(movie.id);
            window.localStorage.movies = storedData;
        }
    };

    const deleteStorage = () => {
        let storedData = window.localStorage.movies.split(',');

        let newData = storedData.filter((id) => id != movie.id);

        window.localStorage.movies = newData;
    };

    return (
        <div className="card">
            <img
                src={
                    movie.poster_path
                        ? 'https://image.tmdb.org/t/p/w500/' + movie.poster_path
                        : './img/poster.jpg'
                }
                alt="Affiche"
            />
            <h2>{movie.title}</h2>

            {/* release_date */}
            {movie.release_date ? (
                <>Sortie le : {dateFormater(movie.release_date)} </>
            ) : (
                <>date de sortie inconnue</>
            )}

            {/* vote_count */}
            {movie.vote_count ? (
                <h4>
                    {movie.vote_average}/10 <span>⭐</span>
                </h4>
            ) : (
                <span>Aucun vote</span>
            )}

            {/* genre_ids */}
            <ul>
                {movie.genre_ids
                    ? listGenres
                    : movie.genres.map((genre, index) => <li key={index}>{genre.name}</li>)}
            </ul>

            {/* overview */}
            {movie.overview ? (
                <>
                    <h3>Synopsis</h3>
                    <p>{movie.overview}</p>
                </>
            ) : (
                ''
            )}

            {/* favoris */}
            {movie.genre_ids ? (
                <div className="btn" onClick={() => addStorage()}>
                    Ajouter aux favoris
                </div>
            ) : (
                <div
                    className="btn"
                    onClick={() => {
                        deleteStorage();
                        window.location.reload();
                    }}
                >
                    Supprimer
                </div>
            )}
        </div>
    );
};

export default Card;
